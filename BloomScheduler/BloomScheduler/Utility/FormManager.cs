﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Threading;

namespace BloomScheduler.Utility
{
    /// <summary>
    /// Manages all forms in the application.
    /// </summary>
    internal static class FormManager
    {
        internal static void CloseForm(Form form)
        {
            if(form != null)
            {
                form.Close();
            }
        }

        internal static void SetFormVisibility(Form form, bool visible)
        {
            if(form != null)
            {
                if (visible)
                {

                    form.Visible = true;
                }
                else
                {
                    form.Visible = false;
                    form = null;
                }
            }

            else
            {
                MessageBox.Show("Error showing form... aborting.");
                Application.Exit();
            }
        }
    }
}
