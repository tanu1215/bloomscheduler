﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomScheduler.Main
{
    /// <summary>
    /// Handles the employees that exist in the application.
    /// </summary>
    internal static class EmployeeHandler
    {
        /// <summary>
        /// Send an email to an employee.
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        internal static void SendEmail(Employee emp, string subject, string body)
        {
            Email.SendEmail(emp.GetEmail(), subject, body);
        }

        internal static Employee GetEmployeeFromId(int id)
        {
            return Employee.GetEmployeeList().FirstOrDefault(employee => employee.GetId() == id);
        }
    }
}
