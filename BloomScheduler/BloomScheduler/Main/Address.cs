﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomScheduler.Main
{
    /// <summary>
    /// Represents a single address for BloomEnergy.
    /// </summary>
    internal class Address
    {
        private string address;

        internal Address(string address)
        {
            this.address = address;
        }

        public override string ToString()
        {
            return this.address;
        }

        /// <summary>
        /// Returns a list of employees that are at the current address.
        /// </summary>
        /// <returns></returns>
        internal List<Employee> GetEmployees(string addressToCheck)
        {
            return Employee.GetEmployeeList().Where(employee => employee.GetCurrentAddress().Equals(addressToCheck)).ToList();
        }
    }
}
