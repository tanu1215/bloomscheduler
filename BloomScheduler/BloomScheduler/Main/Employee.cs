﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BloomScheduler.Data;

namespace BloomScheduler.Main
{
    /// <summary>
    /// Represents an employee.
    /// </summary>
    public class Employee
    {
        private string firstName;
        private string lastName;
        private int id;
        private string email;
        private string currentAddress = "1299 Orleans Dr, Sunnyvale, CA 94089";

        private static int globalID;

        internal static List<Employee> employees = new List<Employee>();

        internal Employee(string firstName, string lastName, string email)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;

            this.id = ++globalID;

            employees.Add(this);
        }

        internal string GetFirstName()
        {
            return firstName;
        }

        internal string GetLastName()
        {
            return lastName;
        }

        internal int GetId()
        {
            return id;
        }

        internal string GetEmail()
        {
            return email;
        }

        internal string GetCurrentAddress()
        {
            return currentAddress;
        }

        internal void SetCurrentAddress(string value)
        {
            currentAddress = value;
        }

        internal static List<Employee> GetEmployeeList()
        {
            return employees;
        }
    }
}
