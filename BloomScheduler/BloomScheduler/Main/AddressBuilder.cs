﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloomScheduler.Main
{
    /// <summary>
    /// Generates the BloomEnergy addresses and the time in minutes between them.
    /// </summary>
    internal static class AddressBuilder
    {
        private static Address[] bloomAddresses = new Address[2];

        static AddressBuilder()
        {
            bloomAddresses[0] = new Address("1299 Orleans Dr, Sunnyvale, CA 94089");
            bloomAddresses[1] = new Address("1330 Bordeaoux Dr, Sunnyvale, Ca 94098");
        }

        /// <summary>
        /// Returns the time, in minutes, between two addresses,
        /// </summary>
        /// <param name="addressOne"></param>
        /// <param name="addressTwo"></param>
        /// <returns></returns>
        internal static int GetTimeBetweenAddresses(string addressOne, string addressTwo)
        {
            string first = bloomAddresses[0].ToString();
            string second = bloomAddresses[1].ToString();

            if (addressOne.Equals(first))
            {
                if(addressTwo.Equals(second))
                {
                    return 10;
                }
            }

            else if(addressOne.Equals(second))
            {
                if(addressTwo.Equals(first))
                {
                    return 10;
                }
            }

            return 0;
        }

        internal static Address[] GetAddresses()
        {
            return bloomAddresses;
        }
    }
}
