﻿namespace BloomScheduler.Forms
{
    partial class CreateTaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateTaskForm));
            this.alertButton = new System.Windows.Forms.Button();
            this.requestButton = new System.Windows.Forms.Button();
            this.meetingButton = new System.Windows.Forms.Button();
            this.aboutButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.schedulerTitleText = new System.Windows.Forms.Label();
            this.bloomTitleText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // alertButton
            // 
            this.alertButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.alertButton.BackColor = System.Drawing.Color.Transparent;
            this.alertButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.alertButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.alertButton.Location = new System.Drawing.Point(784, 208);
            this.alertButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.alertButton.Name = "alertButton";
            this.alertButton.Size = new System.Drawing.Size(293, 107);
            this.alertButton.TabIndex = 23;
            this.alertButton.TabStop = false;
            this.alertButton.Text = "Alert";
            this.alertButton.UseVisualStyleBackColor = false;
            this.alertButton.Click += new System.EventHandler(this.alertButton_Click);
            this.alertButton.MouseHover += new System.EventHandler(this.alertButton_MouseHover);
            // 
            // requestButton
            // 
            this.requestButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.requestButton.BackColor = System.Drawing.Color.Transparent;
            this.requestButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.requestButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.requestButton.Location = new System.Drawing.Point(408, 208);
            this.requestButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.requestButton.Name = "requestButton";
            this.requestButton.Size = new System.Drawing.Size(293, 107);
            this.requestButton.TabIndex = 22;
            this.requestButton.TabStop = false;
            this.requestButton.Text = "Request";
            this.requestButton.UseVisualStyleBackColor = false;
            this.requestButton.Click += new System.EventHandler(this.requestButton_Click);
            this.requestButton.MouseHover += new System.EventHandler(this.requestButton_MouseHover);
            // 
            // meetingButton
            // 
            this.meetingButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.meetingButton.BackColor = System.Drawing.Color.Transparent;
            this.meetingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.meetingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.meetingButton.Location = new System.Drawing.Point(40, 208);
            this.meetingButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.meetingButton.Name = "meetingButton";
            this.meetingButton.Size = new System.Drawing.Size(293, 107);
            this.meetingButton.TabIndex = 20;
            this.meetingButton.TabStop = false;
            this.meetingButton.Text = "Meeting";
            this.meetingButton.UseVisualStyleBackColor = false;
            this.meetingButton.Click += new System.EventHandler(this.meetingButton_Click);
            this.meetingButton.MouseHover += new System.EventHandler(this.meetingButton_MouseHover);
            // 
            // aboutButton
            // 
            this.aboutButton.BackColor = System.Drawing.Color.Transparent;
            this.aboutButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aboutButton.Location = new System.Drawing.Point(878, 496);
            this.aboutButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.aboutButton.Name = "aboutButton";
            this.aboutButton.Size = new System.Drawing.Size(107, 33);
            this.aboutButton.TabIndex = 19;
            this.aboutButton.TabStop = false;
            this.aboutButton.Text = "About";
            this.aboutButton.UseVisualStyleBackColor = false;
            // 
            // helpButton
            // 
            this.helpButton.BackColor = System.Drawing.Color.Transparent;
            this.helpButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.helpButton.Location = new System.Drawing.Point(998, 496);
            this.helpButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(107, 33);
            this.helpButton.TabIndex = 18;
            this.helpButton.TabStop = false;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = false;
            // 
            // schedulerTitleText
            // 
            this.schedulerTitleText.AutoSize = true;
            this.schedulerTitleText.BackColor = System.Drawing.Color.Transparent;
            this.schedulerTitleText.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.schedulerTitleText.ForeColor = System.Drawing.Color.DarkGreen;
            this.schedulerTitleText.Location = new System.Drawing.Point(552, 8);
            this.schedulerTitleText.Name = "schedulerTitleText";
            this.schedulerTitleText.Size = new System.Drawing.Size(293, 59);
            this.schedulerTitleText.TabIndex = 25;
            this.schedulerTitleText.Text = "Scheduler";
            // 
            // bloomTitleText
            // 
            this.bloomTitleText.AutoSize = true;
            this.bloomTitleText.BackColor = System.Drawing.Color.Transparent;
            this.bloomTitleText.Font = new System.Drawing.Font("Verdana", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloomTitleText.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.bloomTitleText.Location = new System.Drawing.Point(376, 8);
            this.bloomTitleText.Name = "bloomTitleText";
            this.bloomTitleText.Size = new System.Drawing.Size(195, 59);
            this.bloomTitleText.TabIndex = 24;
            this.bloomTitleText.Text = "Bloom";
            // 
            // CreateTaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1116, 541);
            this.Controls.Add(this.schedulerTitleText);
            this.Controls.Add(this.bloomTitleText);
            this.Controls.Add(this.alertButton);
            this.Controls.Add(this.requestButton);
            this.Controls.Add(this.meetingButton);
            this.Controls.Add(this.aboutButton);
            this.Controls.Add(this.helpButton);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "CreateTaskForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BloomScheduler";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateTaskForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button alertButton;
        private System.Windows.Forms.Button requestButton;
        private System.Windows.Forms.Button meetingButton;
        private System.Windows.Forms.Button aboutButton;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Label schedulerTitleText;
        private System.Windows.Forms.Label bloomTitleText;
    }
}