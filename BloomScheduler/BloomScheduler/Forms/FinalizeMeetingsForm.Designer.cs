﻿namespace BloomScheduler.Forms
{
    partial class FinalizeMeetingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FinalizeMeetingsForm));
            this.addressDropDownBox = new System.Windows.Forms.ComboBox();
            this.employeeTaskedLabel = new System.Windows.Forms.Label();
            this.addressLabel = new System.Windows.Forms.Label();
            this.enterDateLabel = new System.Windows.Forms.Label();
            this.extraTimeLabel = new System.Windows.Forms.Label();
            this.dateDropDownBox = new System.Windows.Forms.DateTimePicker();
            this.dropDownTimeBox = new System.Windows.Forms.DateTimePicker();
            this.enterTimeLabel = new System.Windows.Forms.Label();
            this.doneButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addressDropDownBox
            // 
            this.addressDropDownBox.BackColor = System.Drawing.SystemColors.Window;
            this.addressDropDownBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.addressDropDownBox.FormattingEnabled = true;
            this.addressDropDownBox.Location = new System.Drawing.Point(64, 72);
            this.addressDropDownBox.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.addressDropDownBox.Name = "addressDropDownBox";
            this.addressDropDownBox.Size = new System.Drawing.Size(232, 26);
            this.addressDropDownBox.TabIndex = 0;
            this.addressDropDownBox.SelectionChangeCommitted += new System.EventHandler(this.addressDropDownBox_SelectionChangeCommitted);
            // 
            // employeeTaskedLabel
            // 
            this.employeeTaskedLabel.AutoSize = true;
            this.employeeTaskedLabel.Location = new System.Drawing.Point(184, 408);
            this.employeeTaskedLabel.Name = "employeeTaskedLabel";
            this.employeeTaskedLabel.Size = new System.Drawing.Size(58, 18);
            this.employeeTaskedLabel.TabIndex = 1;
            this.employeeTaskedLabel.Text = "label1";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(64, 40);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(219, 18);
            this.addressLabel.TabIndex = 2;
            this.addressLabel.Text = "Choose a Bloom Address:";
            // 
            // enterDateLabel
            // 
            this.enterDateLabel.AutoSize = true;
            this.enterDateLabel.Location = new System.Drawing.Point(64, 144);
            this.enterDateLabel.Name = "enterDateLabel";
            this.enterDateLabel.Size = new System.Drawing.Size(117, 18);
            this.enterDateLabel.TabIndex = 4;
            this.enterDateLabel.Text = "Enter a date:";
            // 
            // extraTimeLabel
            // 
            this.extraTimeLabel.AutoSize = true;
            this.extraTimeLabel.Location = new System.Drawing.Point(64, 288);
            this.extraTimeLabel.Name = "extraTimeLabel";
            this.extraTimeLabel.Size = new System.Drawing.Size(58, 18);
            this.extraTimeLabel.TabIndex = 6;
            this.extraTimeLabel.Text = "label1";
            // 
            // dateDropDownBox
            // 
            this.dateDropDownBox.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateDropDownBox.Location = new System.Drawing.Point(64, 168);
            this.dateDropDownBox.Name = "dateDropDownBox";
            this.dateDropDownBox.Size = new System.Drawing.Size(120, 27);
            this.dateDropDownBox.TabIndex = 7;
            this.dateDropDownBox.TabStop = false;
            this.dateDropDownBox.ValueChanged += new System.EventHandler(this.dateDropDownBox_ValueChanged);
            // 
            // dropDownTimeBox
            // 
            this.dropDownTimeBox.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dropDownTimeBox.Location = new System.Drawing.Point(64, 240);
            this.dropDownTimeBox.Name = "dropDownTimeBox";
            this.dropDownTimeBox.Size = new System.Drawing.Size(128, 27);
            this.dropDownTimeBox.TabIndex = 8;
            this.dropDownTimeBox.TabStop = false;
            this.dropDownTimeBox.ValueChanged += new System.EventHandler(this.dropDownTimeBox_ValueChanged);
            // 
            // enterTimeLabel
            // 
            this.enterTimeLabel.AutoSize = true;
            this.enterTimeLabel.Location = new System.Drawing.Point(64, 216);
            this.enterTimeLabel.Name = "enterTimeLabel";
            this.enterTimeLabel.Size = new System.Drawing.Size(117, 18);
            this.enterTimeLabel.TabIndex = 9;
            this.enterTimeLabel.Text = "Enter a time:";
            // 
            // doneButton
            // 
            this.doneButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.doneButton.BackColor = System.Drawing.SystemColors.Window;
            this.doneButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.doneButton.Location = new System.Drawing.Point(64, 328);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(128, 40);
            this.doneButton.TabIndex = 10;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = false;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // FinalizeMeetingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(661, 439);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.enterTimeLabel);
            this.Controls.Add(this.dropDownTimeBox);
            this.Controls.Add(this.dateDropDownBox);
            this.Controls.Add(this.extraTimeLabel);
            this.Controls.Add(this.enterDateLabel);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.employeeTaskedLabel);
            this.Controls.Add(this.addressDropDownBox);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FinalizeMeetingsForm";
            this.Text = "BloomScheduler 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeetingSchedulerForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox addressDropDownBox;
        private System.Windows.Forms.Label employeeTaskedLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.Label enterDateLabel;
        private System.Windows.Forms.Label extraTimeLabel;
        private System.Windows.Forms.DateTimePicker dateDropDownBox;
        private System.Windows.Forms.DateTimePicker dropDownTimeBox;
        private System.Windows.Forms.Label enterTimeLabel;
        private System.Windows.Forms.Button doneButton;
    }
}