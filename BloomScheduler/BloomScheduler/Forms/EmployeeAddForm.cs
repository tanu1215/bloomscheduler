﻿using BloomScheduler.Utility;
using System;
using System.Windows.Forms;
using BloomScheduler.Main;
using BloomScheduler.Data;
using System.Collections.Generic;

namespace BloomScheduler.Forms
{
    internal partial class EmployeeAddForm : Form
    {
        public event EventHandler EmployeeAddedEvent;

        public Employee AddedEmployee { get; private set; }

        private static List<Employee> addedEmployees = new List<Employee>();

        internal static List<Employee> AddedEmployees { get => addedEmployees; }

        internal EmployeeAddForm()
        {
            InitializeComponent();
            this.CenterToScreen();

            this.MaximizeBox = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            string firstnameText = this.firstnameTextBox.Text;
            string lastnameText = this.lastnameTextBox.Text;
            string emailText = this.emailAddressTextBox.Text;

            if (string.IsNullOrWhiteSpace(firstnameText) || string.IsNullOrWhiteSpace(lastnameText) || string.IsNullOrWhiteSpace(emailText))
            {
                MessageBox.Show("You must fill in all fields!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult result = MessageBox.Show("Are you sure?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result != DialogResult.Yes)
            {
                return;
            }

            AddedEmployee = new Employee(firstnameText, lastnameText, emailText.Trim());

            addedEmployees.Add(AddedEmployee);

            //XmlController.Write("Employee", "FirstName", "LastName", "Email", firstnameText, lastnameText, emailText);

            EmployeeAddedEvent?.Invoke(sender, e);

            FormManager.SetFormVisibility(this, false);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            FormManager.SetFormVisibility(this, false);
        }
    }
}
