﻿namespace BloomScheduler.Forms
{
    partial class MeetingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MeetingForm));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.employeeTable = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createMeetingButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.deleteEmployeeButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.employeeTable)).BeginInit();
            this.SuspendLayout();
            // 
            // employeeTable
            // 
            this.employeeTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.employeeTable.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.employeeTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.employeeTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.lastNameColumn,
            this.firstNameColumn,
            this.emailColumn});
            this.employeeTable.Cursor = System.Windows.Forms.Cursors.Hand;
            this.employeeTable.Location = new System.Drawing.Point(8, 16);
            this.employeeTable.Name = "employeeTable";
            this.employeeTable.ReadOnly = true;
            this.employeeTable.Size = new System.Drawing.Size(768, 560);
            this.employeeTable.TabIndex = 40;
            this.employeeTable.TabStop = false;
            // 
            // id
            // 
            this.id.FillWeight = 50.76142F;
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // lastNameColumn
            // 
            this.lastNameColumn.FillWeight = 116.4129F;
            this.lastNameColumn.HeaderText = "Last Name";
            this.lastNameColumn.Name = "lastNameColumn";
            this.lastNameColumn.ReadOnly = true;
            // 
            // firstNameColumn
            // 
            this.firstNameColumn.FillWeight = 116.4129F;
            this.firstNameColumn.HeaderText = "First Name";
            this.firstNameColumn.Name = "firstNameColumn";
            this.firstNameColumn.ReadOnly = true;
            // 
            // emailColumn
            // 
            this.emailColumn.FillWeight = 116.4129F;
            this.emailColumn.HeaderText = "Email";
            this.emailColumn.Name = "emailColumn";
            this.emailColumn.ReadOnly = true;
            // 
            // createMeetingButton
            // 
            this.createMeetingButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.createMeetingButton.BackColor = System.Drawing.Color.Transparent;
            this.createMeetingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.createMeetingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createMeetingButton.Location = new System.Drawing.Point(808, 8);
            this.createMeetingButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.createMeetingButton.Name = "createMeetingButton";
            this.createMeetingButton.Size = new System.Drawing.Size(293, 107);
            this.createMeetingButton.TabIndex = 41;
            this.createMeetingButton.TabStop = false;
            this.createMeetingButton.Text = "Schedule";
            this.createMeetingButton.UseVisualStyleBackColor = false;
            this.createMeetingButton.Click += new System.EventHandler(this.createMeetingButton_Click);
            // 
            // addButton
            // 
            this.addButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.addButton.BackColor = System.Drawing.Color.Transparent;
            this.addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Location = new System.Drawing.Point(808, 136);
            this.addButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(293, 107);
            this.addButton.TabIndex = 42;
            this.addButton.TabStop = false;
            this.addButton.Text = "Add Employee";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.searchButton.BackColor = System.Drawing.Color.Transparent;
            this.searchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.searchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchButton.Location = new System.Drawing.Point(808, 410);
            this.searchButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(293, 107);
            this.searchButton.TabIndex = 44;
            this.searchButton.TabStop = false;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = false;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // deleteEmployeeButton
            // 
            this.deleteEmployeeButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.deleteEmployeeButton.BackColor = System.Drawing.Color.Transparent;
            this.deleteEmployeeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.deleteEmployeeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteEmployeeButton.Location = new System.Drawing.Point(809, 275);
            this.deleteEmployeeButton.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.deleteEmployeeButton.Name = "deleteEmployeeButton";
            this.deleteEmployeeButton.Size = new System.Drawing.Size(293, 107);
            this.deleteEmployeeButton.TabIndex = 45;
            this.deleteEmployeeButton.TabStop = false;
            this.deleteEmployeeButton.Text = "Delete Employee";
            this.deleteEmployeeButton.UseVisualStyleBackColor = false;
            this.deleteEmployeeButton.Click += new System.EventHandler(this.deleteEmployeeButton_Click);
            // 
            // MeetingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1116, 596);
            this.Controls.Add(this.deleteEmployeeButton);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.createMeetingButton);
            this.Controls.Add(this.employeeTable);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "MeetingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BloomScheduler 1.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MeetingForm_FormClosing);
            this.Load += new System.EventHandler(this.MeetingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.employeeTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.DataGridView employeeTable;
        private System.Windows.Forms.Button createMeetingButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailColumn;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Button deleteEmployeeButton;
    }
}