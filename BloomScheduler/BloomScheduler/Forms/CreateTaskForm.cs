﻿using BloomScheduler.Utility;
using System;
using System.Windows.Forms;

namespace BloomScheduler.Forms
{
    internal partial class CreateTaskForm : Form
    {
        internal CreateTaskForm()
        {
            InitializeComponent();

            this.CenterToScreen();
            this.MaximizeBox = false;
        }

        private void CreateTaskForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("TEST!");
        }

        private void alertButton_Click(object sender, EventArgs e)
        {
            //
        }

        private void meetingButton_Click(object sender, EventArgs e)
        {
            FormManager.SetFormVisibility(new MeetingForm(), true);
            FormManager.SetFormVisibility(this, false);
        }

        private void requestButton_Click(object sender, EventArgs e)
        {

        }

        private void alertButton_MouseHover(object sender, EventArgs e)
        {
            toolTip.Show("Send a custom email to an employee.", alertButton);
        }

        private void meetingButton_MouseHover(object sender, EventArgs e)
        {
            toolTip.Show("Schedule a new meeting for an employee.", meetingButton);
        }

        private void requestButton_MouseHover(object sender, EventArgs e)
        {
            toolTip.Show("Create an employee request / job.", requestButton);
        }

        private void backButton_Click_1(object sender, EventArgs e)
        {
            FormManager.SetFormVisibility(new MeetingForm(), true);
            FormManager.SetFormVisibility(this, false);
        }
    }
}
