﻿using BloomScheduler.Main;
using BloomScheduler.Utility;
using System;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using BloomScheduler.Data;
using System.Collections.Generic;

namespace BloomScheduler.Forms
{
    internal partial class MeetingForm : Form
    {
        internal EmployeeAddForm addForm;
        internal FilterForm filterForm;
        internal static string SelectedLastName { get; private set; }
        internal static string SelectedFirstName { get; private set; }

        internal static int Id { get; private set; }
        //internal static Control DataGrid => employeeTable;

        internal MeetingForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;

            this.employeeTable.AllowUserToAddRows = false;
            this.employeeTable.MultiSelect = false;

            if (!XmlController.IsNew)
            {
                foreach(Employee s in XmlController.DisplayReadData())
                {
                    AddEmployeeDataToTable(s);
                }
            }
        }

        private void MeetingForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void createMeetingButton_Click(object sender, EventArgs e)
        {
            if (this.employeeTable.SelectedRows.Count > 0)
            {
                string firstCellValue = this.employeeTable.SelectedRows[0].Cells[0].Value.ToString();
                string secondCellValue = this.employeeTable.SelectedRows[0].Cells[1].Value.ToString();
                string thirdCellValue = this.employeeTable.SelectedRows[0].Cells[2].Value.ToString();
                string fourthCellValue = this.employeeTable.SelectedRows[0].Cells[3].Value.ToString();

                DialogResult result =
                    MessageBox.Show(string.Format("Would you like to create a new meeting for {0} {1}? ",
                        secondCellValue, thirdCellValue), "Confirmation", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);

                switch (result)
                {
                    case DialogResult.Yes:
                        SelectedFirstName = thirdCellValue;
                        SelectedLastName = secondCellValue;
                        Id = int.Parse(firstCellValue);

                        FinalizeMeetingsForm form = new FinalizeMeetingsForm();

                        form.ReciveingForm = this;

                        FormManager.SetFormVisibility(form, true);

                        FormManager.SetFormVisibility(this, false);
                        break;

                    case DialogResult.No:
                        return;
                }
            }

            else
            {
                MessageBox.Show("You must select a row first.", "Error.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            addForm = new EmployeeAddForm();

            addForm.EmployeeAddedEvent += EmployeeDataAdded;

            FormManager.SetFormVisibility(addForm, true);
        }

        private void EmployeeDataAdded(object sender, EventArgs e)
        {
            AddEmployeeDataToTable(addForm.AddedEmployee);
        }

        private void AddEmployeeDataToTable(Employee employee)
        {
            this.employeeTable.Rows.Add(employee.GetId(), employee.GetLastName(), employee.GetFirstName(),
                employee.GetEmail());
        }

        private void AddDummyData()
        {
            foreach (Employee employee in Employee.GetEmployeeList())
            {
                AddEmployeeDataToTable(employee);
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            FormManager.SetFormVisibility(new CreateTaskForm(), true);
            FormManager.SetFormVisibility(this, false);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            filterForm = new FilterForm();

            filterForm.IdFilterRequestedEvent += Id_Query;
            filterForm.EmailFilterRequestedEvent += email_Query;
            filterForm.FirstNameFilterRequestedEvent += firstName_Query;
            filterForm.LastNameFilterRequestedEvent += lastName_Query;

            FormManager.SetFormVisibility(filterForm, true);
        }

        //TODO: Move query code to the FilterForm.

        private void Id_Query(object sender, EventArgs e)
        {
            MessageBox.Show("ID Query requested.");

            try
            {
                Filter(filterForm.IdFilter.ToString(), 0);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void lastName_Query(object sender, EventArgs e)
        {
            MessageBox.Show("Last name Query requested.");

            try
            {
                Filter(filterForm.LastNameFilter, 1);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void firstName_Query(object sender, EventArgs e)
        {
            MessageBox.Show("First name Query requested.");

            try
            {
                Filter(filterForm.FirstNameFilter, 2);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void email_Query(object sender, EventArgs e)
        {
            MessageBox.Show("Email Query requested.");

            try
            {
                Filter(filterForm.EmailFilter, 3);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void Filter(string query, int rowIndex)
        {
            foreach (DataGridViewRow row in this.employeeTable.Rows)
            {
                object cellValue = row.Cells[rowIndex].Value;

                if (cellValue != null && cellValue.ToString().ToUpper().Equals(query.ToUpper()))  
                {
                    row.Selected = true;
                    row.Visible = true;
                }

                else
                {
                    row.Visible = false;
                }
            }
        }

        private void MeetingForm_Load(object sender, EventArgs e)
        {

        }

        private void deleteEmployeeButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure?", "Confirmation.", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.No) return;

            foreach (DataGridViewCell oneCell in this.employeeTable.SelectedCells)
            {
                if (oneCell.Selected)
                {
                    string email = this.employeeTable.SelectedRows[0].Cells[3].Value.ToString().Trim();

                    MessageBox.Show(email);

                    List<Employee> employeeToDelete = new List<Employee>();

                    foreach (Employee employee in EmployeeAddForm.AddedEmployees)
                    {
                        if (email.Trim() == employee.GetEmail())
                        {
                            employeeToDelete.Add(employee);
                        }
                    }

                    employeeToDelete.ForEach(x => EmployeeAddForm.AddedEmployees.Remove(x));

                    if (!XmlController.IsNew)
                    {
                        MessageBox.Show("Deleting element!");

                        XmlController.DeleteElement(email);
                    }

                    this.employeeTable.Rows.RemoveAt(oneCell.RowIndex);
                }
            }
        }
    }
}
