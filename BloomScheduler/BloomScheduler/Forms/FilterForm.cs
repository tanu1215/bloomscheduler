﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BloomScheduler.Utility;

namespace BloomScheduler.Forms
{
    internal partial class FilterForm : Form
    {
        public event EventHandler IdFilterRequestedEvent;
        public event EventHandler EmailFilterRequestedEvent;
        public event EventHandler LastNameFilterRequestedEvent;
        public event EventHandler FirstNameFilterRequestedEvent;

        internal string EmailFilter { get; private set; }
        internal string FirstNameFilter { get; private set; }
        internal string LastNameFilter { get; private set; }
        internal int IdFilter { get; private set; }

        internal FilterForm()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void emailSearchButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(emailTextBox.Text))
            {
                MessageBox.Show("The email field is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                EmailFilter = emailTextBox.Text;
                EmailFilterRequestedEvent?.Invoke(sender, e);
                FormManager.SetFormVisibility(this, false);
            }
        }

        private void lastNameSearchButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lastNameTextBox.Text))
            {
                MessageBox.Show("The last name field is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                LastNameFilter = lastNameTextBox.Text;
                LastNameFilterRequestedEvent?.Invoke(sender, e);
                FormManager.SetFormVisibility(this, false);
            }
        }

        private void firstNameSearchButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(firstNameTextBox.Text))
            {
                MessageBox.Show("The first name field is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                FirstNameFilter = firstNameTextBox.Text;
                FirstNameFilterRequestedEvent?.Invoke(sender, e);
                FormManager.SetFormVisibility(this, false);
            }
        }

        private void idSearchButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(idTextBox.Text))
            {
                MessageBox.Show("The id field is empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                try
                {
                    IdFilter = int.Parse(this.idTextBox.Text);
                    IdFilterRequestedEvent?.Invoke(sender, e);
                    FormManager.SetFormVisibility(this, false);
                }
                catch (Exception)
                {
                    MessageBox.Show("Only integers are allowed!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
