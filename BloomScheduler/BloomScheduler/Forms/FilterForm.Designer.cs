﻿namespace BloomScheduler.Forms
{
    partial class FilterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FilterForm));
            this.idSearchButton = new System.Windows.Forms.Button();
            this.firstNameSearchButton = new System.Windows.Forms.Button();
            this.lastNameSearchButton = new System.Windows.Forms.Button();
            this.emailSearchButton = new System.Windows.Forms.Button();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // idSearchButton
            // 
            this.idSearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idSearchButton.Location = new System.Drawing.Point(368, 40);
            this.idSearchButton.Name = "idSearchButton";
            this.idSearchButton.Size = new System.Drawing.Size(120, 32);
            this.idSearchButton.TabIndex = 1;
            this.idSearchButton.Text = "ID";
            this.idSearchButton.UseVisualStyleBackColor = true;
            this.idSearchButton.Click += new System.EventHandler(this.idSearchButton_Click);
            // 
            // firstNameSearchButton
            // 
            this.firstNameSearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.firstNameSearchButton.Location = new System.Drawing.Point(368, 112);
            this.firstNameSearchButton.Name = "firstNameSearchButton";
            this.firstNameSearchButton.Size = new System.Drawing.Size(120, 32);
            this.firstNameSearchButton.TabIndex = 2;
            this.firstNameSearchButton.Text = "First Name";
            this.firstNameSearchButton.UseVisualStyleBackColor = true;
            this.firstNameSearchButton.Click += new System.EventHandler(this.firstNameSearchButton_Click);
            // 
            // lastNameSearchButton
            // 
            this.lastNameSearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lastNameSearchButton.Location = new System.Drawing.Point(368, 184);
            this.lastNameSearchButton.Name = "lastNameSearchButton";
            this.lastNameSearchButton.Size = new System.Drawing.Size(120, 32);
            this.lastNameSearchButton.TabIndex = 3;
            this.lastNameSearchButton.Text = "Last Name";
            this.lastNameSearchButton.UseVisualStyleBackColor = true;
            this.lastNameSearchButton.Click += new System.EventHandler(this.lastNameSearchButton_Click);
            // 
            // emailSearchButton
            // 
            this.emailSearchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.emailSearchButton.Location = new System.Drawing.Point(368, 256);
            this.emailSearchButton.Name = "emailSearchButton";
            this.emailSearchButton.Size = new System.Drawing.Size(120, 32);
            this.emailSearchButton.TabIndex = 4;
            this.emailSearchButton.Text = "Email";
            this.emailSearchButton.UseVisualStyleBackColor = true;
            this.emailSearchButton.Click += new System.EventHandler(this.emailSearchButton_Click);
            // 
            // idTextBox
            // 
            this.idTextBox.Location = new System.Drawing.Point(40, 40);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(280, 27);
            this.idTextBox.TabIndex = 5;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(40, 256);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(280, 27);
            this.emailTextBox.TabIndex = 6;
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(40, 184);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(280, 27);
            this.lastNameTextBox.TabIndex = 7;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(40, 112);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(280, 27);
            this.firstNameTextBox.TabIndex = 8;
            // 
            // FilterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(603, 350);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(this.emailSearchButton);
            this.Controls.Add(this.lastNameSearchButton);
            this.Controls.Add(this.firstNameSearchButton);
            this.Controls.Add(this.idSearchButton);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FilterForm";
            this.Text = "BloomScheduler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button idSearchButton;
        private System.Windows.Forms.Button firstNameSearchButton;
        private System.Windows.Forms.Button lastNameSearchButton;
        private System.Windows.Forms.Button emailSearchButton;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
    }
}