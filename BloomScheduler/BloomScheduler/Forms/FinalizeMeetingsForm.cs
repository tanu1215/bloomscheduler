﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BloomScheduler.Main;
using BloomScheduler.Utility;

namespace BloomScheduler.Forms
{
    internal partial class FinalizeMeetingsForm : Form
    {
        private string addressSelectedItem;
        private string timeSelectedItem = DateTime.Today.ToShortTimeString();
        private string dateSelectedItem = DateTime.Today.ToShortDateString();

        private readonly Employee employee = EmployeeHandler.GetEmployeeFromId(MeetingForm.Id);

        public Form ReciveingForm { get; set; }

        internal FinalizeMeetingsForm()
        {
            InitializeComponent();
            this.CenterToScreen();
            this.MaximizeBox = false;

            this.extraTimeLabel.Text = string.Empty;

            this.dropDownTimeBox.ShowUpDown = true;
            this.dropDownTimeBox.Format = DateTimePickerFormat.Custom;
            this.dropDownTimeBox.CustomFormat = "hh:mm:tt";

            AddressBuilder.GetAddresses().ToList().ForEach(x => this.addressDropDownBox.Items.Add(x.ToString()));

            string headerNotification = string.Format("Scheduling meeting for {0} {1}...", MeetingForm.SelectedFirstName,
                MeetingForm.SelectedLastName);

            this.employeeTaskedLabel.Text = headerNotification;
        }

        private void MeetingSchedulerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void addressDropDownBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            addressSelectedItem = this.addressDropDownBox.SelectedItem.ToString();
            UpdateTimeDifference();
        }

        private void dropDownTimeBox_ValueChanged(object sender, EventArgs e)
        {
            timeSelectedItem = this.dropDownTimeBox.Value.ToShortTimeString();
            UpdateTimeDifference();
        }

        private void dateDropDownBox_ValueChanged(object sender, EventArgs e)
        {
            dateSelectedItem = this.dateDropDownBox.Value.ToShortDateString();
            UpdateTimeDifference();
        }

        private void UpdateTimeDifference()
        {
            if (!string.IsNullOrEmpty(addressSelectedItem) && !string.IsNullOrEmpty(timeSelectedItem) &&
                !string.IsNullOrEmpty(dateSelectedItem))
            {
                try
                {
                    if (dateSelectedItem.Equals(DateTime.Today.ToShortDateString()))
                    {
                        //TODO GET GOOGLE MAPS TRAVEL DATA!
                        this.extraTimeLabel.Text =
                            "Estimated travel time: +" +
                            AddressBuilder.GetTimeBetweenAddresses(
                                employee.GetCurrentAddress(),
                                addressSelectedItem) + " min.";
                    }

                    else
                    {
                        this.extraTimeLabel.Text = string.Empty;
                    }
                }

                catch (Exception ec)
                {
                    MessageBox.Show(ec.ToString());
                }
            }
        }

        private void doneButton_Click(object sender, EventArgs e)
        {
            Email.SendEmail(employee.GetEmail(), 
                $"Meeting Scheduled for {timeSelectedItem}.", $"Hello, {employee.GetFirstName()}" +
                $" {employee.GetLastName()}. <br> You" +
                $" have a meeting scheduled for {timeSelectedItem} on the date of {dateSelectedItem} " +
                $"at {addressSelectedItem}.");

            MessageBox.Show("Email sent!");

            FormManager.SetFormVisibility(this, true);
            FormManager.SetFormVisibility(ReciveingForm, true);


        }
    }
}
