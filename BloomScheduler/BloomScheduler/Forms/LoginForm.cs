﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using BloomScheduler.Utility;
using BloomScheduler.Main;
using BloomScheduler.Data;

namespace BloomScheduler.Forms
{
    internal partial class LoginForm : Form
    {
        private string initialTextUsername = "Username";
        private string initalTextPassword = "Password";
        private bool userNameGhostEnabled = true;
        private bool passwordGhostEnabled = true;

        internal LoginForm()
        {
            InitializeComponent();

            this.CenterToScreen();

            this.MaximizeBox = false;

            usernameBox.Text = initialTextUsername;
            passwordBox.Text = initalTextPassword;

            usernameBox.ForeColor = Color.Gray;
            passwordBox.ForeColor = Color.Gray;

            usernameBox.GotFocus += RemoveUsernameText;
            usernameBox.LostFocus += AddUsernameText;

            passwordBox.GotFocus += RemovePasswordText;
            passwordBox.LostFocus += AddPasswordText;

            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\text.xml";

            XmlController.Create(path);

            if (XmlController.IsNew)
            {
                XmlController.InitializeDocument("Employees");
            }
        }

        internal void RemoveUsernameText(object sender, EventArgs e)
        {
            if (usernameBox.Text == initialTextUsername)
            {
                usernameBox.Text = string.Empty;
                usernameBox.ForeColor = Color.Black;

                userNameGhostEnabled = false;
            }
        }

        internal void AddUsernameText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(usernameBox.Text))
            {
                usernameBox.Text = initialTextUsername;
                usernameBox.ForeColor = Color.Gray;
                userNameGhostEnabled = true;
            }
        }

        internal void RemovePasswordText(object sender, EventArgs e)
        {
            if (passwordBox.Text == initalTextPassword)
            {
                passwordBox.Text = string.Empty;
                passwordBox.ForeColor = Color.Black;
                passwordBox.PasswordChar = '*';

                passwordGhostEnabled = false;
            }
        }

        internal void AddPasswordText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(passwordBox.Text))
            {
                passwordBox.Text = initalTextPassword;
                passwordBox.ForeColor = Color.Gray;
                passwordBox.PasswordChar = '\0';

                passwordGhostEnabled = true;
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if ((userNameGhostEnabled || passwordGhostEnabled))
            {
                MessageBox.Show("Missing username or password!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FormManager.SetFormVisibility(new MeetingForm(), true);
            FormManager.SetFormVisibility(this, false);
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!XmlController.IsClosed)
            {
                foreach(Employee employee in EmployeeAddForm.AddedEmployees)
                {
                    XmlController.Write("Employee", "FirstName", "LastName", "Email", employee.GetFirstName(), employee.GetLastName(), employee.GetEmail());
                }

                XmlController.Close();
            }
        }
    }
}
