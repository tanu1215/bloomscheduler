﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using BloomScheduler.Main;

namespace BloomScheduler.Data
{
    internal static class XmlController
    {
        public static bool IsClosed { get; private set; }
        public static bool IsNew { get; private set; }

        private static XmlWriter writer;
        private static XDocument doc;
        private static bool isDocumentInitialized;
        private static string filePath;

        internal static void Create(string path)
        {
            filePath = path;

            if (!File.Exists(path))
            {
                XmlWriterSettings settings = new XmlWriterSettings { WriteEndDocumentOnClose = true };

                writer = XmlWriter.Create(path, settings);
                MessageBox.Show(" Made new file! ");

                IsNew = true;
            }
            else
            {
                doc = XDocument.Load(path);
                MessageBox.Show("No new file!");
                IsNew = false;
            }
        }

        internal static void InitializeDocument(string startElement)
        {
            writer.WriteStartDocument();
            //element = employees
            writer.WriteStartElement(startElement); //first super element

            isDocumentInitialized = true;

            MessageBox.Show("Doc initialized!");
        }

        internal static void Write(string element1, string element2, string element3, string element4, string data2, string data3, string data4)
        {
            if (IsNew)
            {
                if (!isDocumentInitialized)
                {
                    Close();
                    throw new AccessViolationException("Document has not been initialized.");
                }

                Console.WriteLine("Document is new!");

                //element1 = Employee
                writer.WriteStartElement(element1); //first element

                //firstname
                writer.WriteStartElement(element2); //second element

                writer.WriteString(data2);

                writer.WriteEndElement();


                //last name
                writer.WriteStartElement(element3);

                writer.WriteString(data3);

                writer.WriteEndElement();


                //email
                writer.WriteStartElement(element4);

                writer.WriteString(data4);

                writer.WriteEndElement();

                writer.WriteEndElement();
            }

            else
            {
                Console.WriteLine("Document is NOT new!");

                try
                {
                    XElement xelement = doc.Element("Employees");
                    xelement.Add(new XElement(element1,
                        new XElement(element2, data2),
                        new XElement(element3, data3),
                        new XElement(element4, data4)));

                    doc.Save(filePath, SaveOptions.None);
                }

                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }

        internal static void Close()
        {
            try
            {
                if (IsNew)
                {
                    writer.Flush();
                    writer.Close();
                }
                else
                {
                    doc.Save(filePath, SaveOptions.None);
                }
            }

            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            IsClosed = true;
        }

        internal static IEnumerable<Employee> DisplayReadData()
        {

            XDocument document = new XDocument(doc);
               

                var emp = from e in document.Descendants("Employee")
                          select new
                          {
                              FirstName = e.Element("FirstName").Value,
                              LastName = e.Element("LastName").Value,
                              Email = e.Element("Email").Value,
                          };


            foreach (var r in emp)
            {
                yield return new Employee(r.FirstName, r.LastName, r.Email);
            }
        }

        internal static void DeleteElement(string email)
        {
            string target = email.Trim();
            doc.Descendants("Employees")
               .Elements("Employee")
               .Where(x => x.Element("Email").Value.Equals(target))
               .Remove();

            MessageBox.Show($"Deleted {target}");
        }
    }
}